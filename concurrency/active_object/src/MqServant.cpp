#include "MqServant.hpp"
#include "Message.hpp"
#include <cstddef>
#include <stdexcept>

MqServant::MqServant(std::size_t mq_size)
    : max_size_ { mq_size }
    , messages_ {}
{
}

void MqServant::put(Message const& msg)
{
    if (full()) {
        throw std::length_error("put error: Message queue full");
    }

    messages_.push(msg);
}

Message MqServant::get()
{
    if (empty()) {
        throw std::length_error("get error: Message queue empty");
    }

    const auto message { messages_.front() };
    messages_.pop();
    return message;
}

bool MqServant::empty() const
{
    return messages_.empty();
}

bool MqServant::full() const
{
    return messages_.size() == max_size_;
}
