# Active Object


> The Active Object design pattern decouples method execution from method invocation to
> enhance concurrency and simplify synchronized access to objects that reside in their own
> threads of control.

Q: What is the difference between "method invocation" and "method execution"?


## Structure

- A *proxy* provides an interface that allows clients to invoke publicly-accessible methods on an active object.

### Class diagram
![Active Object Class Diagram](./res/active_object.png "Active Object: Class Diagram")

## Dynamic

Three phases of the Active Object pattern:

1. Method request construction and scheduling. 
    - A client invokes a method on the proxy, triggering the creation of a method request.
    - The method request maintains:
        + the argument bindings to the method
        + and any other bindings needed to execute the method and return its result.
    - The proxy passses the method request to its scheduler, which enqueues it on the activation queue.
        + If the method is defined as a two-way invocation, a future is returned to the client.
        + If the method is a one-way (has no return values), then no future is returned.
2. Method request execution
    - The active object's scheduler runs continuously in a different thread than its clients.
    - The scheduler monitors its activation list and determines which method request(s) have become runnable by calling their guard method.
    - When a method request becomes runnable, the scheduler removes it, binds the request to its servant, and dispatches the appropriate method on the servant.
    - When this method is called, it can access and update the state of its servant and create its result if it is a two-way method invocation.
3. Completion
    - The result (if any) is stored in the future
    - The active object's scheduler returns to monitor the activation list for runnable method requests
    - After a two-way methods completes, clients can retrieve its result via the future.
