#pragma once

#include <cstddef>
#include <limits>
#include <queue>

#include "Message.hpp"

class MqServant {
public:
    MqServant(std::size_t mq_size = std::numeric_limits<std::size_t>::max());

    // Message queue operations
    void put(const Message& msg);
    Message get();

    // Predicates
    bool empty() const;
    bool full() const;

private:
    const std::size_t max_size_;
    std::queue<Message> messages_;
};
