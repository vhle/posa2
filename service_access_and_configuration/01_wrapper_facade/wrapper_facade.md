# Wrapper Facade

> Encapsulate non-object-oriented functions and data structures with a cohesive object-oriented abstraction.

## Potential benefits

1. More concise code using higher level features, e.g., constructors, destructors, exceptions, and garbage collection.
2. More portable software
3. More maintainable software
4. More cohesive components

## Structure and dynamics


![Structure of the wrapper facade pattern](./res/wrapper_facade_structure.png "Structure of Wrapper Facade Pattern")

![Interaction among the participations in the wrapper facade pattern](./res/wrapper_facade_dynamics.png "Interaction among the participations in the wrapper facade pattern")


## Implementation

1. Identify the cohesive abstractions and relationships among low-level APIs
2. Cluster cohesive groups of functions into wrapper facade classes and methods
    a. Create cohesive classes
    b. Coalesce multiple individual functions into a single method
    c. Automate creation and destruction operations, if possible
    d. Select the level of indirection
    e. Determine where to encapsulate platform-specific variations

## Consequences

+ Concise, cohesive, and robust higher-level object-oriented programming interfaces.
+ Portability and maintainability
+ Modularity, reusability, and configurability

- Loss of functionality
- Performance degradation
- Programming language and compiler limitations
