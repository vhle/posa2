#pragma once

#include <cstdint>
#include <cstring>
#include <netinet/in.h>
#include <sys/socket.h>

class InetAddress {
public:
    InetAddress(std::uint16_t port, std::uint32_t addr)
    {
        memset(&addr_, 0, sizeof(addr_));
        addr_.sin_family = AF_INET;
        addr_.sin_port = htons(port);
        addr_.sin_addr.s_addr = htonl(addr);
    }

    std::uint16_t get_port() const
    {
        return addr_.sin_port;
    }

    std::uint32_t get_ip_addr() const
    {
        return addr_.sin_addr.s_addr;
    }

    sockaddr const& addr() const
    {
        return reinterpret_cast<sockaddr const&>(addr_);
    }

    std::size_t size() const
    {
        return sizeof(addr_);
    }

private:
    sockaddr_in addr_;
};
