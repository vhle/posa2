#pragma once

// Since C++11, we can use std::condion_variable

#include "thread_mutex.hpp"
#include <bits/types/struct_timespec.h>
#include <chrono>
#include <pthread.h>
#include <system_error>
class thread_condition {
public:
    thread_condition(thread_mutex& m)
        : mutex_ { m }
    {
        const auto error_code { pthread_cond_init(&cond_, nullptr) };
        if (error_code != 0) {
            throw std::system_error { error_code, std::system_category() };
        }
    }

    ~thread_condition()
    {
        pthread_cond_destroy(&cond_);
    }

    thread_condition(thread_condition const&) = delete;
    thread_condition(thread_condition&&) = delete;
    thread_condition& operator=(thread_condition const&) = delete;
    thread_condition& operator=(thread_condition&&) = delete;

    void wait(std::chrono::nanoseconds timeout = std::chrono::nanoseconds { 0 })
    {
        auto error_code { 0 };
        if (timeout == std::chrono::nanoseconds { 0 }) {
            error_code = pthread_cond_wait(&cond_, &mutex_.mutex_);
        } else {
            const auto abstime {
                timespec {
                    .tv_sec = timeout.count() / 1'000'000'000,
                    .tv_nsec = timeout.count() % 1'000'000'000 }
            };
            error_code = pthread_cond_timedwait(&cond_, &mutex_.mutex_, &abstime);
        }

        if (error_code != 0) {
            throw std::system_error { error_code, std::system_category() };
        }
    }

    void notify()
    {
        // cond_ is ensured to be innitialized in the constructor,
        // so pthread_cond_signal will not fail
        (void)pthread_cond_signal(&cond_);
    }

    void notify_all()
    {
        // cond_ is ensured to be innitialized in the constructor,
        // so pthread_cond_broadcast will not fail
        (void)pthread_cond_broadcast(&cond_);
    }

private:
    pthread_cond_t cond_;
    thread_mutex& mutex_;
};
