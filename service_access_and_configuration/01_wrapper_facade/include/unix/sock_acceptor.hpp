#pragma once

#include "inet_addr.hpp"
#include "sock_stream.hpp"
#include "socket.hpp"
#include <cerrno>
#include <sys/socket.h>
#include <system_error>

class SockAcceptor {
public:
    SockAcceptor(InetAddress const& addr)
    {
        // Create a local endpoint of communication
        handle_ = ::socket(PF_INET, SOCK_STREAM, 0);

        // Associate addr with endpoint
        if (::bind(handle_, &addr.addr(), addr.size()) == -1) {
            throw std::system_error(std::error_code(errno, std::system_category()));
        }

        // Make endpoint listen for connections
        static constexpr int Backlog { 5 };
        ::listen(handle_, Backlog);
    }

    // Initialize a passive-mode acceptor socket
    void open(InetAddress const& addr)
    {
        // Create a local endpoint of communication
        handle_ = ::socket(PF_INET, SOCK_STREAM, 0);

        // Associate addr with endpoint
        if (::bind(handle_, &addr.addr(), addr.size()) == -1) {
            throw std::system_error(std::error_code(errno, std::system_category()));
        }

        // Make endpoint listen for connections
        static constexpr int Backlog { 5 };
        ::listen(handle_, Backlog);
    }

    void accept(SockStream& s)
    {
        s.set_handle(::accept(handle_, 0, 0));
    }

private:
    Socket handle_;
};
