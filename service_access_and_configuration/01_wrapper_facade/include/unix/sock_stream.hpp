#pragma once

#include "socket.hpp"
#include <cstddef>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

class SockStream {
public:
    SockStream()
        : handle_ { InvalidHandleValue }
    {
    }
    SockStream(Socket h)
        : handle_ { h }
    {
    }
    ~SockStream() { close(handle_); }

    void set_handle(Socket h)
    {
        handle_ = h;
    }

    Socket get_handle() const
    {
        return handle_;
    }

    ssize_t recv(void* buf, size_t len, int flags)
    {
        return ::recv(handle_, buf, len, flags);
    }

    ssize_t send(unsigned char const* buf, size_t len, int flags)
    {
        return ::send(handle_, buf, len, flags);
    }

    ssize_t recv_n(unsigned char* buf, size_t len, int flags)
    {
        return 0; // TODO
    }

    ssize_t send_n(unsigned char const* buf, size_t len, int flags)
    {
        return 0; // TODO
    }

private:
    Socket handle_;
};

