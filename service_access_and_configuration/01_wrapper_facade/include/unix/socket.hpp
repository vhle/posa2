#pragma once

using Socket = int;
constexpr Socket InvalidHandleValue { -1 };
