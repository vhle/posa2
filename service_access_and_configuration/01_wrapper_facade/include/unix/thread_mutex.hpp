#pragma once

#include <pthread.h>
#include <system_error>

// For C++11 or newer, std::mutex can be used instead
class thread_mutex {
public:
    thread_mutex()
    {
        const auto error_code { pthread_mutex_init(&mutex_, nullptr) };
        if (error_code != 0) {
            throw std::system_error { std::error_code { error_code, std::system_category() } };
        }
    }

    ~thread_mutex()
    {
        pthread_mutex_destroy(&mutex_);
    }

    thread_mutex(thread_mutex const&) = delete;
    thread_mutex(thread_mutex&&) = delete;
    thread_mutex& operator=(thread_mutex const&) = delete;
    thread_mutex& operator=(thread_mutex&&) = delete;

    void acquire()
    {
        const auto error_code { pthread_mutex_lock(&mutex_) };
        if (error_code != 0) {
            throw std::system_error { std::error_code { error_code, std::system_category() } };
        }
    }

    void release()
    {
        const auto error_code { pthread_mutex_unlock(&mutex_) };
        if (error_code != 0) {
            throw std::system_error { std::error_code { error_code, std::system_category() } };
        }
    }

private:
    pthread_mutex_t mutex_;

    friend class thread_condition;
};
