# Component Configurator


# Structure

![Component Configurator: Class Diagram](./res/component_configurator_structure.png "Component Configurator Class Diagram")

![Component Configurator: Sequence Diagram](./res/component_configurator_sequence.png "Component Configurator Sequence Diagram")

![Component Configurator: Component State Diagram](./res/component_state_diagram.png "Component State Diagram")

There are 4 participants in the component configurator pattern:

1. A *component* defines an interface used to configure and control the application service/functionality provided by a component implementation.
2. *Concrete components* implement the component control interface. They are packaged in the form that can be dynamically linked and unlinked into or out of an application run-time, such as DLL.
3. A *component repository* manages all concrete components, allowing system management applications or administrators to control the behavior of the configured concrete components via a central administrative mechanism.
4. A *component configurator* uses the component respository to coordinate the configuration of concrete components.

