#include "cristian.hpp"
#include "component.hpp"

extern "C" {
Component* make_time_server()
{
    return new CristianTimeServer();
}
}
