#pragma once

#include "component.hpp"
#include <iostream>
#include <string>

class CristianTimeServer : public Component {
public:
    void init() override
    {
        std::cout << "CristianTimeServer::init()\n";
    }

    void fini() override
    {
        std::cout << "CristianTimeServer::fini()";
    }

    void suspend() override
    {
        std::cout << "CristianTimeServer::suspend()\n";
    }

    void resume() override
    {
        std::cout << "CristianTimeServer::resume()\n";
    }

    std::string info() const override
    {
        return std::string { "Mock CristianTimeServer" };
    }

    void handle_event(DllHandle, ComponentConfiguratorEventType) override
    {
        std::cout << "CristianTimeServer::handle_event\n";
    }
};
