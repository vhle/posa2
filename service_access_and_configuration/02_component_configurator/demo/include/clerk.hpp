#pragma once

#include "component.hpp"
#include <iostream>

class Clerk : public Component {

    void init() override
    {
        std::cout << "Clerk::init\n";
    }

    void fini() override
    {
        std::cout << "Clerk::fini()\n";
    }
};
