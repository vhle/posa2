#pragma once

#include "component.hpp"
#include <iostream>
#include <string>

class BerkeleyTimeServer : public Component {
public:
    void init() override
    {
        std::cout << "BerkeleyTimeServer::init()\n";
    }

    void fini() override
    {
        std::cout << "BerkeleyTimeServer::fini()";
    }

    void suspend() override
    {
        std::cout << "BerkeleyTimeServer::suspend()\n";
    }

    void resume() override
    {
        std::cout << "BerkeleyTimeServer::resume()\n";
    }

    std::string info() const override
    {
        return std::string { "Mock BerkeleyTimeServer" };
    }

    void handle_event(DllHandle, ComponentConfiguratorEventType) override
    {
        std::cout << "BerkeleyTimeServer::handle_event\n";
    }
};
