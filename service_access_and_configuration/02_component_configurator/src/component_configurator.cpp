#include "component_configurator.hpp"
#include <iostream>

ComponentConfigurator::ComponentConfigurator()
{
    std::cout << "ComponentConfigurator::ComponentConfigurator\n";
}

ComponentConfigurator::~ComponentConfigurator()
{
    std::cout << "ComponentConfigurator::~ComponentConfigurator\n";
}

ComponentConfigurator* ComponentConfigurator::instance()
{
    static ComponentConfigurator instance;
    return &instance;
}

void ComponentConfigurator::process_directive(std::string const& directive)
{
    std::cout << "ComponentConfigurator::process_directive: " << directive << '\n';
}
