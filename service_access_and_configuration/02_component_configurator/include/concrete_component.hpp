#pragma once

#include "component.hpp"
#include "component_configurator.hpp"
#include <string>

template <typename COMPONENT>
class ConcreteComponent {
public:
    static COMPONENT* instance(std::string const& name)
    {
        Component* c = ComponentConfigurator::instance()->component_respository()->find(name);
        return dynamic_cast<COMPONENT*>(c);
    }
};
