#pragma once

#include "component_configurator_event_type.hpp"
#include "dll.hpp"
#include <string>

class Component {
public:
    // Initialization and termination hooks
    virtual void init() = 0;
    virtual void fini() = 0;

    // Scheduling hooks
    virtual void suspend();
    virtual void resume();

    // Status information hook
    virtual std::string info() const = 0;

    virtual void handle_event(DllHandle, ComponentConfiguratorEventType) = 0;
};
