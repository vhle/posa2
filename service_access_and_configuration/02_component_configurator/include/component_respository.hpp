#pragma once

#include "component.hpp"
#include <memory>
#include <string>
class ComponentRepository {
public:
    ComponentRepository();
    ComponentRepository(ComponentRepository const&) = delete;
    ComponentRepository(ComponentRepository&&) = delete;
    ComponentRepository& operator=(ComponentRepository const&) = delete;
    ComponentRepository& operator=(ComponentRepository&&) = delete;

    ~ComponentRepository();

    void insert(std::string const& component_name, std::unique_ptr<Component>);
    Component* find(std::string const& component_name);
    void remove(std::string const& component_name);
    void suspend(std::string const& component_name);
    void resume(std::string const& component_name);

private:
};
