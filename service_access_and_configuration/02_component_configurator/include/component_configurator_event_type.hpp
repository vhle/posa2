#pragma once

enum class ComponentConfiguratorEventType {
    Init,
    Suspend,
    Resume,
    Fini,
};
