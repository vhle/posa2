#pragma once

// This is an implementation for UNIX only
// In a real cross-platform library, there typically implementation for other platforms

#include <dlfcn.h>
#include <stdexcept>
#include <string>

using DllHandle = void*;

class Dll {
    Dll(std::string const& dll_name)
    {
        handle_ = ::dlopen(dll_name.c_str(), 0);
        if (handle_ == NULL) {
            throw std::runtime_error(std::string { "Failed to open DLL: " } + dll_name);
        }
    }

    Dll(Dll const&) = delete;
    Dll(Dll&&) = delete;
    Dll& operator=(Dll const&) = delete;
    Dll& operator=(Dll&&) = delete;

    ~Dll()
    {
        dlclose(handle_);
    }

    /// If `symbol_name` is in the symbol table of the DLL
    /// return a pointer to it, nullptr otherwise
    void* symbol(std::string const& symbol_name)
    {
        return dlsym(handle_, symbol_name.c_str());
    }

private:
    DllHandle handle_;
};
