#pragma once

#include "component_respository.hpp"
#include <string>

class ComponentConfigurator {
public:
    ComponentConfigurator();
    ComponentConfigurator(ComponentConfigurator const&) = delete;
    ComponentConfigurator(ComponentConfigurator&&) = delete;
    ComponentConfigurator& operator=(ComponentConfigurator const&) = delete;
    ComponentConfigurator& operator=(ComponentConfigurator&&) = delete;
    ~ComponentConfigurator();

    void process_directive(std::string const& directive_string);
    ComponentRepository* component_respository();

    static ComponentConfigurator* instance();

private:
};
