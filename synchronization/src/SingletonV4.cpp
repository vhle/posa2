#include "SingletonV4.hpp"
#include <atomic>
#include <mutex>

std::atomic<SingletonV4*> SingletonV4::s_instance;
std::mutex SingletonV4::s_mutex;

SingletonV4& SingletonV4::get_instance()
{
    SingletonV4* p = s_instance.load(std::memory_order_acquire);
    if (p == nullptr) { // 1st check
        std::lock_guard lock { s_mutex };
        p = s_instance.load(std::memory_order_relaxed);
        if (p == nullptr) { // 2nd check
            p = new SingletonV4 {};
            s_instance.store(p, std::memory_order_release);
        }
    }
    return *p;
}
