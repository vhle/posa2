#include "SingletonV3.hpp"
#include <mutex>
#include <optional>

std::optional<SingletonV3> SingletonV3::s_instance;
std::once_flag SingletonV3::s_once_flag;

SingletonV3& SingletonV3::get_instance()
{
    std::call_once(SingletonV3::s_once_flag,
        []() {
            s_instance.emplace(SingletonV3 {});
        });

    return *s_instance;
}
