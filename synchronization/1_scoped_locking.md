# Scoped Locking

Core idea: Use RAII for managing locks.

Benefits:

- Increased robustness

Liabilities:

- Potential for deadlock when used recursively.
- Limitation with language-specific semantics:
    + requires RAII feature
    + `longjmp(), thread_exit()` break this pattern.



