# Thread-Safe Interfaces

> The Thread-Safe Interface design pattern minimizes locking overhead and ensures that intra-component method calls do not incur ‘self-deadlock’ by trying to reacquire a lock that is held by the component already.

## Method

Structure all components that process intra-component method invocations according to 2 design conventions:

- Interface methods check: 
    1. An interface method acquires a lock
    2. The interface method then forwards to implementation methods, which perform the actual work.
    3. The interface method then releases the lock.

- Implementation methods trust: Implementation methods should only perform work when called by interface methods; they trust that they are called with necessary lock(s) lock held and should never acquire or release locks by themselves.

## Variants

- Thread-safe facade
- Thread-safe wrapper facade

## Consequences

### Benefits

- Increased robustness
- Enhanced performance
- Simplification of software

### Liabilities

- Additional indirection and extra methods
- Potential deadlock: This pattern does not completely resolve deadlock by itself. Deadlock can still happen at the client site.
- Potential for misuse: Objects of the same Thread-safe interface can invoke private methods of other objects in the same class => Do not do this.
- Potential overhead:
    + May increase the number of locks needed, increasing synchronization overhead and making it harder to detect and avoid deadlocks.
    + Prevent locking at a finer granulity than the component, which can increase lock contention.


