#pragma once

template <typename LOCK>
class Guard {
public:
    explicit Guard(LOCK& l)
        : lock_ { l }
    {
        lock_.acquire();
        owner_ = true;
    }

    ~Guard()
    {
        if (owner_) {
            lock_.release();
        }
    }

private:
    LOCK& lock_;
    bool owner_ { false };
};
