#pragma once

#include "GuardTemplate.hpp"
#include <string>

template <class LOCK>
class FileCache2 {
public:
    explicit FileCache2(LOCK& lock)
        : lock_ { lock }
    {
    }

    const void* lookup(std::string const& path) const
    {
        Guard<LOCK> guard { lock_ };
        return lookup_i(path);
    }

    void insert(std::string const& path)
    {
        Guard<LOCK> guard { lock_ };
        insert_i(path);
    }

private:
    LOCK& lock_;

    const void* lookup_i(std::string const& path)
    {
        // The actual implementation, no acquire/release here
        return nullptr;
    }

    void insert_i(std::string const& path)
    {
        // ...
    }

    // Other private methods that do not do acquire/release here
};
