#pragma once

#include "SingletonV3.hpp"
#include <mutex>
#include <optional>

class SingletonV3 {
    static SingletonV3& get_instance();
private:
    static std::optional<SingletonV3> s_instance;
    static std::once_flag s_once_flag;

    SingletonV3() { }
};
