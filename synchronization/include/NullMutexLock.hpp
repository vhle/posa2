#pragma once

/// A polymorphic no-op lock
#include "Lock.hpp"

class NullMutexLock : public Lock {

public:
    void acquire() override { }

    void release() override { }
};
