#pragma once

#include "Guard.hpp"
#include <string>

class FileCache {
    FileCache(Lock& l)
        : lock_ { l }
    {
    }

    const void* lookup(std::string const& path) const
    {
        Guard guard { lock_ };

        // Implement the lookup method

        return nullptr;
    }

private:
    Lock& lock_;
};
