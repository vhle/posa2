#pragma once

// Singleton implementation using memory fence

#include <atomic>
#include <mutex>

class SingletonV4 {
public:
    static SingletonV4& get_instance();

private:
    SingletonV4() = default;
    static std::atomic<SingletonV4*> s_instance;
    static std::mutex s_mutex;
};
