#pragma once

class SingletonV2 {

    static SingletonV2& get_instance()
    {
        static SingletonV2 instance;
        return instance;
    }

private:
    SingletonV2() { }
};
