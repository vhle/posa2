#pragma once

#include "GuardTemplate.hpp"
#include <string>

template <typename LOCK>
class FileCache {

public:
    explicit FileCache(LOCK& l)
        : lock_ { l }
    {
    }

    const void* lookup(std::string const& path) const
    {
        Guard<LOCK> guard { lock_ };

        // Implement the lookup method

        return nullptr;
    }

private:
    LOCK& lock_;
};
