#pragma once

// This idiom has become a stanard in C++11 with std::lock_guard and
// std::mutex. Here we re-implement it for learning purpose.

#include "thread_mutex.hpp"

/// The object should be created and destroyed in
/// a code block in the same thread, so there will be no
/// race condition on owner_ flag in acquire() and release()
class ThreadMutexGuard {
public:
    ThreadMutexGuard(thread_mutex& mutex)
        : mutex_ { mutex }
    {
        acquire();
    }

    ~ThreadMutexGuard()
    {
        release();
    }

    ThreadMutexGuard(ThreadMutexGuard const&) = delete;
    ThreadMutexGuard(ThreadMutexGuard&&) = delete;
    ThreadMutexGuard& operator=(ThreadMutexGuard const&) = delete;
    ThreadMutexGuard& operator=(ThreadMutexGuard&&) = delete;

    void acquire()
    {
        mutex_.acquire();
        owner_ = true; // Only set to true after the previous statement
    }

    void release()
    {
        if (owner_) {
            owner_ = false;
            mutex_.release();
        }
    }

private:
    thread_mutex& mutex_;
    bool owner_ { false };
};
