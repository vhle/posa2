#pragma once

#include "Lock.hpp"

class Guard {
public:
    explicit Guard(Lock& lock)
        : lock_ { lock }
    {
        lock.acquire();
        owner_ = true;
    }

    ~Guard()
    {
        if (owner_) {
            lock_.release();
        }
    }

private:
    Lock& lock_;
    bool owner_ { false };
};
