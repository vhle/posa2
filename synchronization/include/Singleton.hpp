#pragma once

#include <mutex>

class Singleton {
public:
    static Singleton* instance()
    {
        std::lock_guard guard { mutex_ };
        if (instance_ == nullptr) {
            instance_ = new Singleton();
        }

        return instance_;
    }

private:
    static std::mutex mutex_;
    static Singleton* instance_;

    Singleton()
    {
    }
};
