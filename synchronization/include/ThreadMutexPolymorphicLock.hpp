#pragma once

#include "Lock.hpp"
#include "thread_mutex.hpp"

class ThreadMutexPolymorphicLock : public Lock {

public:
    void acquire() override { lock_.acquire(); }

    void release() override { lock_.release(); }

private:
    thread_mutex lock_;
};
