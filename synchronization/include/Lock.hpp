#pragma once
#include "thread_mutex.hpp"

class Lock {
public:
    virtual void acquire() = 0;
    virtual void release() = 0;
    virtual ~Lock() = default;
};
