#pragma once

// A non-polymorphic no-op lock,
// to be used with GuardTemplate in single-threaded programs.
class NullMutex {
    void acquire() { }

    void release() { }
};
