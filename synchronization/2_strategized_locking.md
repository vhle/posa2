# Strategized Locking

> The Strategized Locking design pattern parameterizes synchronization mechanisms that protect a component’s critical sections from concurrent access.

## Benefits

- Enhanced flexibility and customization
- Decreased maintenance effort for components
- Improved reuse

## Liabilities

- Obtrusive locking: The template-based method exposes the locking strateties to application code.

- Over-engineering: In-experienced developers may use the wrong type of locks for a certain application.
