add_library(synchronization STATIC 
    src/ThreadMutexGuard.cpp
    src/SingletonV3.cpp
    src/SingletonV4.cpp
    )
target_include_directories(synchronization PUBLIC include)
target_link_libraries(synchronization PUBLIC thread_wrapper)
